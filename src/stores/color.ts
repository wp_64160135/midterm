import { ref } from "vue";
import { defineStore } from "pinia";

export const useColorStore = defineStore("color", () => {
  const color1 = ref();
  const color2 = ref();
  const color3 = ref();
  const color4 = ref();
  const color5 = ref();
  const color6 = ref();

  function changeColor1() {
    color1.value = "yellow";
    color2.value = "none";
    color3.value = "none";
    color4.value = "none";
    color5.value = "none";
    color6.value = "none";
  }
  function changeColor2() {
    color1.value = "none";
    color2.value = "yellow";
    color3.value = "none";
    color4.value = "none";
    color5.value = "none";
    color6.value = "none";
  }
  function changeColor3() {
    color1.value = "none";
    color2.value = "none";
    color3.value = "yellow";
    color4.value = "none";
    color5.value = "none";
    color6.value = "none";
  }
  function changeColor4() {
    color1.value = "none";
    color2.value = "none";
    color3.value = "none";
    color4.value = "yellow";
    color5.value = "none";
    color6.value = "none";
  }
  function changeColor5() {
    color1.value = "none";
    color2.value = "none";
    color3.value = "none";
    color4.value = "none";
    color5.value = "yellow";
    color6.value = "none";
  }
  function changeColor6() {
    color1.value = "none";
    color2.value = "none";
    color3.value = "none";
    color4.value = "none";
    color5.value = "none";
    color6.value = "yellow";
  }
  return {
    changeColor1,
    changeColor2,
    changeColor3,
    changeColor4,
    changeColor5,
    changeColor6,
    color1,
    color2,
    color3,
    color4,
    color5,
    color6,
  };
});
